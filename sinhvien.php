<?php 
    class SinhVien{
        private $conn;

        private $table_name="sinhvien";

        public $maSV;
        public $tenSinhVien;
        public $diaChi;
        public $namSinh;
        public $gioiTinh;

        public function __construct($db)
        {
          $this->conn= $db;  
        }
        function read(){
            $query= "SELECT * FROM ".$this->table_name."";
           // prepare là phương thức bảo mật, tranh sql injection
            $stmt= $this->conn->prepare($query);
            $stmt->execute();
            return $stmt;
        }
        function insert(){
            $query= "INSERT INTO ".$this->table_name." 
                  (maSV,tensinhvien,diachi,namsinh,gioitinh)
            value(null,:tensinhvien,:diachi,:namsinh,:gioitinh)";
            $stmt= $this->conn->prepare($query);
            
            $this->tenSinhVien=htmlspecialchars(strip_tags($this->tenSinhVien));
            $this->diaChi=htmlspecialchars(strip_tags($this->diaChi));
            $this->namSinh=htmlspecialchars(strip_tags($this->namSinh));
            $this->gioiTinh=htmlspecialchars(strip_tags($this->gioiTinh));
           

            $stmt->bindParam(':tensinhvien', $this->tenSinhVien);
            $stmt->bindParam(':diachi', $this->diaChi);
            $stmt->bindParam(':namsinh', $this->namSinh);
            $stmt->bindParam(':gioitinh', $this->gioiTinh);
           
            if($stmt->execute()){
                return true;
            }
            return false;

        }

        function update(){
            $query= "UPDATE ".$this->table_name."
                        SET tensinhvien= :tensinhvien,
                            diachi=:diachi,
                            namsinh=:namsinh,
                            gioitinh=:gioitinh
                        WHERE
                            maSV= :maSV";

            $stmt= $this->conn->prepare($query);

            $this->tenSinhVien=htmlspecialchars(strip_tags($this->tenSinhVien));
            $this->diaChi=htmlspecialchars(strip_tags($this->diaChi));
            $this->namSinh=htmlspecialchars(strip_tags($this->namSinh));
            $this->gioiTinh=htmlspecialchars(strip_tags($this->gioiTinh));
            $this->maSV=htmlspecialchars(strip_tags($this->maSV));

            $stmt->bindParam(':tensinhvien', $this->tenSinhVien);
            $stmt->bindParam(':diachi', $this->diaChi);
            $stmt->bindParam(':namsinh', $this->namSinh);
            $stmt->bindParam(':gioitinh', $this->gioiTinh);
            $stmt->bindParam(':maSV', $this->maSV);

            if($stmt->execute()){
                return true;
            }
            return false;
            

        }
        function delete(){
            $query="DELETE FROM ".$this->table_name." WHERE maSV= ?";
            $stmt= $this->conn->prepare($query);
            $this->maSV= htmlspecialchars(strip_tags($this->maSV));
            $stmt->bindParam(1, $this->maSV);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        function search($keywords){
            $query="SELECT * FROM ".$this->table_name." WHERE tensinhvien LIKE ? OR maSV LIKE ?";
        $stmt = $this->conn->prepare($query);
        $keywords = htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";
        //bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        
        $stmt->execute();
        return $stmt;

        }

    }
?>