<?php
    header("Content-type: text/html; charset=utf-8");
    class Database{
        private $host="localhost";
        private $db_name="api_sinhvien";
        private $user_name="root";
        private $password="";
        public $conn;
        
        public function getConnection(){
           $this->conn=null;
            try{
                $this->conn = new PDO('mysql:host='.$this->host.';dbname='.$this->db_name,$this->user_name, $this->password,
                array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8",
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    ));
                
                
            }catch(PDOException $exception){
                echo "Connecttion error: ".$exception->getMessage();

            }
           
            return $this->conn;
        }
    }
?>
