<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    require_once "../config/core.php";
    require_once "../config/database.php";
    require_once "../sinhvien.php";

    $database= new Database();
    $db = $database->getConnection();

    $sinhvien = new SinhVien($db);

    $keyword = isset($_GET["string"])? $_GET["string"]: "";

    $stmt= $sinhvien->search($keyword);
    $num = $stmt->rowCount();

    if($num>0){
        $sinhvien_array=array();
        $sinhvien_array["records"]=array();
        while($row= $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $sinhvien_item=array(
                "masinhvien"=>$maSV,
                "tensinhvien"=>$tensinhvien,
                "diachi"=>$diachi,
                "namsinh"=>$namsinh,
                "gioitinh"=>$gioitinh
            );
            array_push($sinhvien_array["records"],$sinhvien_item);

        }
        http_response_code(200);
        $e= json_encode($sinhvien_array);
        echo $e;

    }else{
        http_response_code(404);
        echo json_encode(
            array("message" => "Khong tim thay sinh vien.")
        );
    }





?>