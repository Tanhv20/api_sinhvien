<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require_once "../config/database.php";
    require_once "../sinhvien.php";

    $database = new Database();
    $db = $database->getConnection();
    
    $sinhvien = new SinhVien($db);
    $data= json_decode(file_get_contents("php://input"));

    $sinhvien->maSV = $data->masinhvien;
    $sinhvien->tenSinhVien = $data->tensinhvien;
    $sinhvien->namSinh= $data->namsinh;
    $sinhvien->diaChi = $data->diachi;
    $sinhvien->gioiTinh= $data->gioitinh;
    if($sinhvien->update()){
        http_response_code(200);
        // tell the user
        echo json_encode(array("message" => "Sinhvien was updated."));
    }else{
  
        // set response code - 503 service unavailable
        http_response_code(503);
      
        // tell the user
        echo json_encode(array("message" => "Unable to update sinhvien."));
    }
