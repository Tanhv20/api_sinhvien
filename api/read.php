<?php
header("Content-type: text/html; charset=utf-8");
    require_once "../config/database.php";
    require_once "../sinhvien.php";

    $database= new Database();
    $db= $database->getConnection();
    $db->exec("set names utf8");
    $sinhvien = new SinhVien($db);
    $stmt = $sinhvien->read();
    $num = $stmt->rowCount();
    if($num>0){
        $sinhvien_array= array();
        $sinhvien_array["records"]=array();
        while($row= $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $sinhvien_item=array(
                "masinhvien"=>$maSV,
                "tensinhvien"=>$tensinhvien,
                "diachi"=>$diachi,
                "namsinh"=>$namsinh,
                "gioitinh"=>$gioitinh
            );
            array_push($sinhvien_array["records"],$sinhvien_item);
            
        }
        http_response_code(200);
        $obj_e= json_encode($sinhvien_array,true);
        $obj= json_decode($obj_e,true);
        echo print_r($obj_e);
        //echo "</br>";
        // foreach($obj["records"] as $key=>$value){
        //    echo $value["masinhvien"] ."</br>";
        //    echo $value["tensinhvien"] ."</br>";
        //    echo $value["diachi"]."</br>";
        //    echo $value["namsinh"]."</br>";
        //    echo $value["gioitinh"]."</br>";
        //    echo "--------------------------------------------------------- </br>";
        // }
    }
?>